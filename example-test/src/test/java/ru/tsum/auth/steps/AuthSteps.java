package ru.tsum.auth.steps;

import net.thucydides.core.annotations.Step;
import ru.tsum.auth.pages.AuthPage;
import ru.tsum.auth.pages.ResultPage;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthSteps {
    AuthPage authPage;
    ResultPage resultPage;

    @Step
    public void openTsumAuthPage() {
        authPage.open();
    }

    @Step
    public void inputLoginData(String login, String password) {
        authPage.logIn(login, password);
    }

    @Step
    public void shouldSuccesfulAuthorizedToTsumWebsite() {
        String resultTitle = resultPage.getResultPageName();

        assertThat(resultTitle).isEqualTo("Мои данные");
    }

    @Step
    public void shouldAppearAllAlertsForEmptyOnCurrentPage() {
        String baseAlert = authPage.getHeadAlert();
        String emailAlert = authPage.getEmailAlert();
        String passAlert = authPage.getPassAlert();

        assertThat(baseAlert).isEqualTo("Введите email и пароль");
        assertThat(emailAlert).isEqualTo("Неверный e-mail");
        assertThat(passAlert).isEqualTo("Введите пароль");
    }

    @Step
    public void shouldAppearHeadAlertOnCurrentPage() {
        String baseAlert = authPage.getHeadAlert();

        assertThat(baseAlert).isEqualTo("Неверный логин или пароль");
    }
}
