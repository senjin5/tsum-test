package ru.tsum.auth.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.tsum.ru/login")
public class AuthPage extends PageObject {
    @FindBy(css = ".auth-layout .button__text")
    WebElement logInButton;
    @FindBy(css = ".auth-layout [formcontrolname='email']")
    WebElement emailField;
    @FindBy(css = ".auth-layout [formcontrolname='password']")
    WebElement passwordField;
    @FindBy(css = ".auth-layout .login__result")
    WebElement headAlert;
    @FindBy(css = ".auth-layout .popup__input:nth-of-type(2) .field__error")
    WebElement emailAlert;
    @FindBy(css = ".auth-layout .popup__input:nth-of-type(3) .field__error")
    WebElement passwordAlert;


    public void logIn(String login, String password) {
        emailField.sendKeys(login);
        passwordField.sendKeys(password);

        logInButton.click();
    }

    public String getHeadAlert() {
        return headAlert.getText();
    }


    public String getEmailAlert() {
        return emailAlert.getText();
    }


    public String getPassAlert() {
        return passwordAlert.getText();
    }
}
