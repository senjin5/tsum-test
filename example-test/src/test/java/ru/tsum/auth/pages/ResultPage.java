package ru.tsum.auth.pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultPage extends PageObject {
    @FindBy(css = ".h2")
    WebElement pageName;

    public String getResultPageName() {
        return pageName.getText();
    }
}
