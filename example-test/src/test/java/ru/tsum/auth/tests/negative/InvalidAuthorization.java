package ru.tsum.auth.tests.negative;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import ru.tsum.auth.steps.AuthSteps;

@RunWith(SerenityRunner.class)
public class InvalidAuthorization {
    @Managed(driver = "chrome", uniqueSession = true)
    WebDriver driver;

    @Steps
    AuthSteps auth;

    @Test
    public void shouldUnsuccesfulAuthorizedToTsumWebsiteWithEmptyPasswordAndEmail() {
        auth.openTsumAuthPage();
        auth.inputLoginData("", "");
        auth.shouldAppearAllAlertsForEmptyOnCurrentPage();
    }

    @Test
    public void shouldUnsuccesfulAuthorizedToTsumWebsiteWithValidEmailAndAnyCasePassword() {
        auth.openTsumAuthPage();
        auth.inputLoginData("pavel.gritcenko@yandex.ru", "SenJinPasS");
        auth.shouldAppearHeadAlertOnCurrentPage();
    }
}
