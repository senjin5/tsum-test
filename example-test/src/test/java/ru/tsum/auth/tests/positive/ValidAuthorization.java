package ru.tsum.auth.tests.positive;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import ru.tsum.auth.steps.AuthSteps;

@RunWith(SerenityRunner.class)
public class ValidAuthorization {
    @Managed(driver = "chrome", uniqueSession = true)
    WebDriver driver;

    @Steps
    AuthSteps auth;

    @Test
    public void shouldSuccesfulAuthorizedToTsumWebsiteWithValidData() {
        auth.openTsumAuthPage();
        auth.inputLoginData("pavel.gritcenko@yandex.ru", "senjinPass");
        auth.shouldSuccesfulAuthorizedToTsumWebsite();
    }

    @Test
    public void shouldSuccessfulAuthorizedToTsumWithValidDataAnyCaseValidEmail() {
        auth.openTsumAuthPage();
        auth.inputLoginData("Pavel.Gritcenko@YANDEX.RU", "senjinPass");
        auth.shouldSuccesfulAuthorizedToTsumWebsite();
    }
}
